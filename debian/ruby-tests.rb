# These tests were already skipped in the original ruby-test-files.yaml
SKIPPED_TESTS = [
  './spec/jruby/benchmarks/shootout_bm.rb',
  './spec/jruby/benchmarks/symbolize_keys_bm.rb',
  './spec/jruby/unpacker_spec.rb',
]

# See https://bugs.debian.org/1088785
SKIPPED_TESTS_PPC64EL = [
  './spec/./msgpack_spec.rb',
  './spec/./packer_spec.rb',
  './spec/./unpacker_spec.rb',
]

arch=RbConfig::CONFIG["arch"]
success=true
Dir.glob("./spec/{*,.}/*.rb").each do |f|
  next if SKIPPED_TESTS.include?(f)
  if SKIPPED_TESTS_PPC64EL.include?(f) and arch == "powerpc64le-linux-gnu"
    puts "I: Skipping #{f} on #{arch}"
    next
  end
  puts "-------- running #{f} test ------- "
  system("#{ENV['RUBY_TEST_BIN']} -I#{ENV['RUBYLIB']} #{f}") or begin
    puts "E: Test #{f} has failed."
    success=false
  end
end

exit(1) unless(success)
